
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Mstring.h"


// Lire une chaine a partir de clavier
void readString(char *chaine )
{
    char c; 
    int  i = 0; 

    
    while(  ( c = getchar() ) != '\n' )
    {
        switch ( c )
        {
        case '\b': // Le de boutton effacer
            i--;
            *( chaine + i) = ' ';
            break;
        case '\\': // Le cas d'une anti slash
            *( chaine + i) = c;
            i++;
            *( chaine + i) = c;
            i++;
            break;
        default: // Les cas normal
            *( chaine + i ) = c;
            i++;
            break;
        }
    } 

    *( chaine + i) = '\0'; 

}

// Supprimer une chaine dans une grande chaine

int chainepos(const char* chaine, const char* sous_chaine)
{
    
    int i, j, pos;
     i = j = 0; pos = -1;
    
    while ( i < strlen(sous_chaine) )
    {
          if ( chaine[ j ] == sous_chaine[ i ])
          {
               if ( i == 0)
                  pos = j;
               i++;
               j++;
          }
          else
          {
              pos = -1;
              i = 0;
              j++;
          }
    } 
    
    return pos;
     
}

void supchaine(char* chaine, const char* sous_chaine)
{
     int pos, i, j;
     char chaine1[ 100 ];
     char chaine2[ 100 ];
     
     pos = chainepos(chaine, sous_chaine);
     
     for ( i = 0; i < pos; i++)
     {
         chaine1[ i ] = chaine[ i ];
     }
     
     chaine1[ i ] = '\0';
     printf("\nsupchaine->chaine1 : %s\n",chaine1);
        
     j = 0;
     i = pos + strlen( sous_chaine );
     
     for ( ; i < strlen( chaine ); i++)
     {
         if ( j !=0)
         {
            chaine2[ j ] = chaine[ i ];
            j++;
         }
         else
         {
             if ( chaine[ i ] != ' ' )
             {
                 chaine2[ j ] = chaine[ i ];
                 j++;  
             }
             else
                 continue;
         }
     }
     
     chaine2[ j ] = '\0';
     printf("\nsupchaine->chaine2 : %s\n",chaine2);

     sprintf( chaine,"%s%s", chaine1, chaine2);
}


