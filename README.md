**Note:** This readme file is in French language.

# Le m�canique de programme #

Dans notre programme on utilise les id�es suivantes:

* Pour les informations enregistrer dans les fichiers, on sauvegard�  des fiche  composer de trois champs: le champ Nom, le champ Pr�nom, le champ Age. 
* Pour distinguer entre un champ et un autre on utilise  le caract�re ';'. 
* Pour distinguer entre une fiche et une autre  on utilise le  caract�re '$'.
* Pour la recherche on utilise le Nom et le Pr�nom comme  cl�.
* Pour l'insertion on insert � la fin de fichier (dans le dernier bloc).
* Pour la suppression on utilise la suppression physique des articles (fiches).

Pour garantie un bon d�roulement de programme on construire les proc�dures et les fonctions suivantes:

## Les proc�dures ##

* void   lireenrg(const Tbloc buffer, char *chaine, int pos):    lire une fiche dans le bloc buffer a la position pos et met le dans la varaible de sortie chaine.
* void   initbuffer(Tbloc *buffer) : initialiser un bloc buffer.
* void   rempenrg(Tbloc *buffer,int pos, char *chaine, int length) :   ranger une ficher dans un buffer.
* void   cherginit( const char* nomfichier ) :  cette procedure est le plus importante parsqu'il est le createur des fichier en premier temps. Il demande les fiche a enregistrer dans le fichier et les ranger dans des bloc dans le fichier.
* void   affiche_ficheir( const char* nomfichier ) : parcourer tous le fichier bloc par bloc et affichier a l'ecran le nombre de bloc current , le nombre des fiches dans cette bloc et enfinle champ suiv de bloc et les fiches sauvgarder.
* void   rech( char* nom, char* prenomc,const char* nomfichier, boolean* trouv, int* i, int* j ) : fait la recherche de fiche qui a la m�me champ Nom et Pr�nomc et retourner le nombre de bloc et le nombre de fiche dans cette bloc et un boolean qui pr�ciser  si la fiche est trouvable ou non.
* void   ins_enrg( char* enrg, char* nomfichier) :  ins�rer une fiche si non trouver dans a la fin de fichier.
* void   sup_enrg( char* nomc, char* prenomc, char* nomfichier) : la suppression des fiche est physique. Si la fiche existe dans le fichier (on utilise le proc�dure rech pour cette tache) alors on supprime la fiche, on d�caler les fiche de bloc, on lire le dernier fiche dans le fichier, on compare s'il y a d'espace dans le bloc ou se trouve la fichier � supprimer, si c'est le cas en supprimer la derni�re fiche de fichier de la dernier bloc et on insert le dans le fichier ou se trouve la fiche � supprimer. Sinon on fait rein en plus de suppression de fiche demander. Si la fiche est non trouver on fait rein.
* void readString(char *chaine ) : lire une chaine � partir de clavier.
* int  chainepos(const char* chaine, const char* sous_chaine) : donner la position de la chaine sou-chaine dans la chaine chaine.
* void supchaine(char* chaine, const char* sous_chaine) : supprimer la chaine sous-chaine de la chaine chaine.

## Les types ##

### La structure des blocs ###


```
#!pseudo code

Tbloc = structure
{
    Tenrg : chaine[100];
    Nb : entier;
    Suiv : entier;
} 
```

### La structure de bloc entete ###


```
#!pseudo code

Entete = structure
{
     Tete : entier;
} 
```

Dans cette programme on utilise les proc�dures de machine abstraite des fichiers suivant :
La proc�dure d'ouverture, fermeture, liredir, ecriredir.