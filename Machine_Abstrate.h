
#define  TAILLE_BLOC    100

typedef  struct    Tbloc
{
    char Tenrg[ TAILLE_BLOC ];
    int nb;
    int suiv;

} Tbloc;

typedef  struct    Entete
{
         int tete;
         
} Entete;




int    ouvrir(FILE* f, const char *nomfichier, char mode);
void   fermer( FILE *f);
void   liredir( Tbloc* buffer, int i, FILE *f );
void   ecriredir( const Tbloc buffer, int i, FILE* f );
void   lireseq( FILE *f, Tbloc *buf );
void   ecrirseq( FILE *f, Tbloc *buf );
void   initbuffer(Tbloc *buffer);
void   rempenrg(Tbloc *buffer,int pos, char *chaine, int length);
void   lireenrg(const Tbloc buffer, char *chaine, int pos);
void   aff_entete( FILE* f, int n_caracteristique, int caracteristique);
int    entete(FILE* f, int n_caracteristique);
