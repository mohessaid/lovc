
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Machine_Abstrate.h"




// ouvrir ou cree un fichier
int   ouvrir(FILE *f, const char *nomfichier, char mode)
{
    if ( mode == 'a' )
    {
        f = fopen(nomfichier, "r+");
    }
    else if ( mode == 'n' )
    {
        f = fopen(nomfichier, "w+");
        printf("HI hi hi!!!:  %p\n", f);
    }
    if ( f != NULL)
        return 0;
    else
        return 1;
}

// fermer un fichier
void   fermer( FILE *f)
{

    printf("%ld", fclose(f) );
}

// lire un blog a la position i dans le fichier f
void   liredir( Tbloc* buffer, int i, FILE *f )
{
    fseek(f, sizeof(Tbloc)*i,SEEK_SET);
    fread((Tbloc *)buffer, sizeof(Tbloc),1, f);
    //printf("fseek %ld", ferror(f));
}

// ecrir un bloc dans un fichier a la position i
void   ecriredir( const Tbloc buffer, int i, FILE *f )
{
    fseek(f, sizeof(Tbloc)*i,SEEK_SET);

    fwrite(&buffer, sizeof(Tbloc),1, f);
    //printf("fseek %ld", ferror(f));
}

// lire le bloc current
void   lireseq( FILE *f, Tbloc *buf )
{
    fseek(f, sizeof(Tbloc), SEEK_CUR);

    fread(buf, sizeof(Tbloc),1, f);

}

// ecrire un bloc dans la position current dans f
void   ecrirseq( FILE *f, Tbloc *buf )
{
    fseek(f, sizeof(Tbloc), SEEK_CUR);
    fwrite(buf, sizeof(Tbloc),1, f);
}

// initailizer un buffer
void   initbuffer(Tbloc *buffer)
{
    int i;

    for( i = 0; i  <= TAILLE_BLOC; i++)
    {
        buffer->Tenrg[i] = 0;
    }
    buffer->suiv = 0;
    buffer->nb = 0;
}

void   rempenrg(Tbloc *buffer,int pos, char *chaine, int length)
{
    int i;

    for ( i = 0; i <= length; i++ )
    {
        (*buffer).Tenrg[pos + i] = chaine[i];
    }
}

void   lireenrg(const Tbloc buffer, char *chaine, int pos)
{
    char c = 'a';
    char ch[ TAILLE_BLOC ]; 
    int i = 1, j = 0;
    
    for ( j = 0; j <= TAILLE_BLOC; j++)
    { 
        ch[j] = buffer.Tenrg[ j ];
    }
    //printf("ch = %s", ch);
    j = 0; 
    while (  ( i < pos ))
    {
        while (c != '$')
        {
            c = ch[j++];
            //j++;
        }
        c = ch[j];
        i++; 
    }
    
    i = 0; 
    while ( c != '$')
    {
        c = ch[j];
        *(chaine + i) = c;
        j++;
        i++;
    }
    *(chaine + i) = '\0';
    printf("\nlireenrg->chaine: %s\n", chaine);
}

void aff_entete( FILE* f, int n_caracteristique, int caracteristique)
{
     Entete buf;
     
     if  ( n_caracteristique == 1 )
         buf.tete = caracteristique;
     
     
     fseek(f, 0 , SEEK_SET);
     fwrite(&buf,sizeof( Entete ),1,f);
     
     
}

int  entete(FILE* f, int n_caracteristique)
{
     Entete buf;
     
     
     fseek(f, 0 , SEEK_SET);
     fread(&buf,sizeof( Entete ),1,f);
     
     if ( n_caracteristique == 1 )
        return  buf.tete;  
     
}
